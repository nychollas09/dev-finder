import { desenvolvedorSchema } from "../domain/models/Desenvolvedor";
import { URL_GITHUB_API } from "../contants";
import { definirLocalizacao } from "../utils/PointSchema";
import axios from "axios";

export class DesenvolvedorService {
  async buscarTodosOsDesenvolvedores(response) {
    console.log("aqui");
    return response.json(await desenvolvedorSchema.find());
  }

  async buscarDesenvolvedoresPorCompetencias(request, response) {}

  async cadastrarDesenvolvedor(request, response) {
    let { nome_github, competencias, latitude, longitude } = request.body;

    let desenvolvedor = await desenvolvedorSchema.findOne({ nome_github });

    if (!desenvolvedor) {
      competencias = competencias
        .split(",")
        .map(competencia => competencia.trim());

      const apiResponse = await axios.get(URL_GITHUB_API + `/${nome_github}`);

      const { name = login, avatar_url, bio } = apiResponse.data;

      const localizacao = definirLocalizacao(latitude, longitude);

      desenvolvedor = await desenvolvedorSchema.create({
        nome_github,
        nome: name,
        avatar_url,
        biografia: bio,
        competencias,
        localizacao
      });
    }

    return response.json(desenvolvedor);
  }
}
