import { PORT, URL_CONNECT_DB } from './contants';
import { desenvolvedorRoutes } from './resource/desenvolvedor.routes';
import express from 'express';
import moongose from 'mongoose';

const application = express();

moongose.connect(URL_CONNECT_DB, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

application.use(express.json());

application.use(desenvolvedorRoutes);

application.listen(PORT);

console.log('Pronto!');