import { Router } from 'express';
import { DesenvolvedorService } from '../service/desenvolvedor.service';

export const desenvolvedorRoutes = Router();
const desenvolvedorService = new DesenvolvedorService();

desenvolvedorRoutes.get('/desenvolvedor', (request, response) => {
    if(request.query){
        return desenvolvedorService.buscarDesenvolvedoresPorCompetencias(request, response);
    }
    return desenvolvedorService.buscarTodosOsDesenvolvedores(response);
});

desenvolvedorRoutes.post('/desenvolvedor', (request, response) => {
    return desenvolvedorService.cadastrarDesenvolvedor(request, response);
});