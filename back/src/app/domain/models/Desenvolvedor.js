import mongoose from 'mongoose';
import { PointSchema } from '../../utils/PointSchema';

const DesenvolvedorSchema = new mongoose.Schema({
    nome: String,
    nome_github: String,
    biografia: String,
    avatar_url: String,
    competencias: new Array(String),
    localizacao: {
        type: PointSchema,
        indice: '2dsphere'
    }
});

export const desenvolvedorSchema = mongoose.model('desenvolvedor', DesenvolvedorSchema);