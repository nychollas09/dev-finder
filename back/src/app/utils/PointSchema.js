import mongoose from 'mongoose';

export const PointSchema = new mongoose.Schema({
    type: {
        type: String,
        enum: ['Point'],
        required: true
    },
    coordenadas: {
        type: [Number],
        required: true
    }
});

export function definirLocalizacao(latitude, longitude){
    return {
        type: 'Point',
        coordenadas: [longitude, latitude]
    };
}